
const fs = require('fs');
const path = require('path');
const EOL = require('os').EOL;

const LOG_FILE_PATH = path.join(__dirname, 'logs', 'app.log');

let consoleOut = true;
let needEOL = true;

const getTimestamp = (msg) => {
  if(!msg) return '';
  return needEOL ? '' : `${Date.now()} `;
};

const getMessageDisplay = (msg) => {
  let ts = getTimestamp(msg);
  msg = msg || '';
  return `${ts}${msg}`;
};

// flags: a = open for appending; create if DNE
const writer = fs.createWriteStream(LOG_FILE_PATH, {flags: 'a'});
const writeMsg = (msg) => {
  writer.write(msg);
  if(consoleOut) process.stdout.write(msg);
};

writer.write(EOL+'----------------------------------------------'+EOL);

module.exports = {
  logToConsole: (doit) => consoleOut = doit,
  log: (msg) => {
    msg = getMessageDisplay(msg);
    writeMsg(`${msg}`);
    needEOL = true;
  },
  logLine: (msg) => {
    msg = getMessageDisplay(msg);
    writeMsg(`${msg}${EOL}`);
    needEOL = false;
  },
  close: () => {
    try {
      writer.end();
    }
    catch(e) {
      // nothing
    }
  }
};