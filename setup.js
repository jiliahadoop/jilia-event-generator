const path = require('path');
require('dotenv').config({
  path: path.join(__dirname, '.env')
});

const logger = require('./app-logger');
const e = process.env;

const TENANT_NAMES = e.TENANTS
  .split(',')
  .filter((s) => s)
  .map((s) => s.trim());

if(TENANT_NAMES.length === 0) {
  logger.logLine('ERROR: Missing "TENANTS" env var');
  logger.close();
  process.exit(1);
}
module.exports.TENANT_NAMES = TENANT_NAMES;

const HUBS_PER_TENANT = parseInt(e.HUBS_PER_TENANT) || 0;
if(isNaN(HUBS_PER_TENANT) || HUBS_PER_TENANT <= 0) {
  logger.logLine('ERROR: Missing valid "HUBS_PER_TENANT" env var');
  logger.close();
  process.exit(1);
}
module.exports.HUBS_PER_TENANT = HUBS_PER_TENANT;

const EVENT_INTERVAL = parseInt(e.EVENT_INTERVAL) || 0;
if(isNaN(HUBS_PER_TENANT) || HUBS_PER_TENANT <= 0) {
  logger.logLine('ERROR: Missing valid "EVENT_INTERVAL" env var');
  logger.close();
  process.exit(1);
}
module.exports.EVENT_INTERVAL = EVENT_INTERVAL;

logger.logLine();
logger.logLine(`Tenants Count: ${TENANT_NAMES.length}`);
TENANT_NAMES.forEach((n) => logger.logLine(`  ${n}`));
logger.logLine();
logger.logLine(`Number of hubs per tenant: ${HUBS_PER_TENANT}`);
logger.logLine(`Milliseconds between each hub event: ${EVENT_INTERVAL}`);
module.exports.STATS_UPDATE = parseInt(e.STATS_DISPLAY_UPDATE) || 5000;
logger.logLine(`Stats Display Update Interval: ${module.exports.STATS_UPDATE}`);
logger.logLine();

const totalHubsCount = TENANT_NAMES.length * HUBS_PER_TENANT;
const totalEventsMin = ((1000/EVENT_INTERVAL) * 60) * totalHubsCount;
const totalEventsHour = Math.round(totalEventsMin * 60);
const totalEventsDay = Math.round(totalEventsMin * 60 * 24);
const totalEventsMonth = Math.round(totalEventsMin * 60 * 24 * 30);
const totalEventsYear = Math.round(totalEventsMin * 60 * 24 * 365);

logger.logLine(`Total Hubs: ${totalHubsCount}`);
logger.logLine(`Total Events Per Minute: ${Math.round(totalEventsMin)}`);
logger.logLine(`Total Events Per Hour: ${totalEventsHour}`);
logger.logLine(`Total Events Per Day: ${totalEventsDay}`);
logger.logLine(`Total Events Per Month: ${totalEventsMonth}`);
logger.logLine(`Total Events Per Year: ${totalEventsYear}`);
logger.logLine();

module.exports.LOG_EVENTS = parseInt(e.LOG_EVENTS) === 1;
logger.logLine(`Logging Events? ${module.exports.LOG_EVENTS}`);

module.exports.DISPLAY_COUNTER = parseInt(e.DISPLAY_COUNTER) === 1;
logger.logLine(`Displaying Events Count? ${module.exports.DISPLAY_COUNTER}`);

module.exports.SOCKETS_PER_TENANT = parseInt(e.SOCKETS_PER_TENANT) || 1;
logger.logLine();
logger.logLine(`Number of Sockets Per Tenant:  ${module.exports.SOCKETS_PER_TENANT}`);
logger.logLine(`Total Socket Connections: ${TENANT_NAMES.length * module.exports.SOCKETS_PER_TENANT}`);


module.exports.MOCK_SOCKET = parseInt(e.MOCK_SOCKET) === 1;
logger.logLine(`Using Mock Socket? ${module.exports.MOCK_SOCKET}`);

module.exports.USE_TLS = parseInt(e.USE_TLS) === 1;
logger.logLine(`Using TLS Socket? ${module.exports.USE_TLS}`);

module.exports.MAX_RECONNECT_ATTEMPTS = parseInt(e.MAX_RECONNECT_ATTEMPTS);
logger.logLine(`Socket Reconnect Attempts: ${module.exports.MAX_RECONNECT_ATTEMPTS}`);
module.exports.RECONNECT_ATTEMPT_DELAY = parseInt(e.RECONNECT_ATTEMPT_DELAY);
logger.logLine(`Socket Reconnect Delay: ${module.exports.RECONNECT_ATTEMPT_DELAY}`);

logger.logLine();

Object.freeze(module.exports);