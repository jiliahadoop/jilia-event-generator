const path = require('path');
require('dotenv').config({
  path: path.join(__dirname, '.env-test-server')
});

const net = require('net');
const EOL = require('os').EOL;

const chance = require('./chance-inst');

const PORT = process.env.PORT;
const SIM_DISCONNECTS = parseInt(process.env.DO_CLIENT_DISCONNECTS) === 1;

let clientIdCounter = 1;
// let clientsCounter = 0;
let messagesCounter = 0;

let clients = [];

const server = net.createServer((client) => {

  client.id = `#${clientIdCounter++}`;
  clients.push(client);
  console.log(`Client ${client.id} Connect`);

  client.once('close', () => {
    let idx = clients.findIndex((c) => c.id == client.id);
    if(idx !== -1) {
      clients.splice(idx, 1);
      console.log(`Client ${client.id} Removed`);
    }
    client = null;
  });

  let incompleteMsg = '';
  client.on('data', (data) => {
    let msg = data.toString();
    if(incompleteMsg) {
      msg += incompleteMsg;
    }
    if(msg[msg.length-1] === EOL) {
      messagesCounter++;
      incompleteMsg = '';
      return;
    }
    incompleteMsg += msg;
  });
});

server.listen({port:PORT, host: 'localhost'}, () => console.log(`server listening on ${PORT}`));

let lastMsgsCount = 0;
setInterval(() => {
  server.getConnections((err, count) => {
    if(err) return console.error(err);
    if(count > 0) {
      let delta = messagesCounter - lastMsgsCount;
      console.log(`Connections: ${count}  -  Messages: ${messagesCounter} (+${delta})`);
      lastMsgsCount = messagesCounter;
    }
  });
}, 60000);

if(SIM_DISCONNECTS) {

  const DISCONNECT_TIMES = process.env.CLIENT_DISCONNECT_TIME_RANGE
    .split(',')
    .filter((n)=>n)
    .map((n) => parseInt(n));

  if(DISCONNECT_TIMES.length !== 2) {
    console.error('Invalid DISCONNET_TIME_RANGE env var');
    process.exit(1);
  }
  console.log('Simulating Disconnects');

  const getNextDisconnectTime = () => {
    return chance.integer({
      min: DISCONNECT_TIMES[0],
      max: DISCONNECT_TIMES[1]
    });
  };

  // const rejectConnections = () => {
  //   server.maxConnections = -1;
  //   setTimeout(() => {
  //     server.maxConnections = Number.MAX_VALUE;
  //   },  chance.integer({min:10000, max:35000}));
  // };

  const doDisconnect = () => {
    if(clients.length > 0) {
      let client = chance.pickone(clients);
      console.log(`Disconnecting client ${client.id}`);
      client.destroy();
      // Reject reconnects for a bit
      // rejectConnections();
    }

    setTimeout(doDisconnect, getNextDisconnectTime());
  };

  setTimeout(doDisconnect, getNextDisconnectTime());
}

process.on('SIGINT', () => {
  console.log('SIGINT');
  server.close(() => process.exit(0));
  setTimeout(() => process.exit(0), 1000);
});