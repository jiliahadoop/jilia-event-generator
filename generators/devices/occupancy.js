const chance = require('../../chance-inst');

const deviceType = 'occupancy';

module.exports = {
  create: () => new ctor()
};

const motions = ['Active', 'Inactive'];
let idx = 0;
const nextMotion = () => {
  idx = idx ? 0 : 1;
  return motions[idx];
};
const streams = {
  batteryVoltage: () => chance.floating({min: 0.1, max: 3, fixed: 1}),
  currentTemperature: () => chance.floating({min: 12, max: 30, fixed: 2}),
  motion: () => nextMotion()
};
const streamNames = ['batteryVoltage', 'currentTemperature', 'motion'];
const streamWeights = [15, 1512, 68];

function ctor() {
  const id = chance.guid();

  this.getEvent = function(){
    let streamName = chance.weighted(streamNames, streamWeights);
    let streamValue = streams[streamName]();
    return {
      device: id,
      deviceType: deviceType,
      stream: streamName,
      streamValue: streamValue
    };
  };
}