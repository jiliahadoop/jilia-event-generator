const chance = require('../../chance-inst');

const deviceType = 'contact';

module.exports = {
  create: () => new ctor()
};

const states = ['Open', 'Closed'];
let idx = 0;
const nextStatus = () => {
  idx = idx ? 0 : 1;
  return states[idx];
};

const streams = {
  status: () => nextStatus(),
  batteryVoltage: () => chance.floating({min: 1.1, max: 2.3, fixed: 1}),
  currentTemperature: () => chance.floating({min: 12, max: 30, fixed: 2})
};

const streamNames = ['status', 'batteryVoltage', 'currentTemperature'];
const streamWeights = [32, 15, 1080];

function ctor() {
  const id = chance.guid();

  this.getEvent = function(){
    let streamName = chance.weighted(streamNames, streamWeights);
    let streamValue = streams[streamName]();
    return {
      device: id,
      deviceType: deviceType,
      stream: streamName,
      streamValue: streamValue
    };
  };
}