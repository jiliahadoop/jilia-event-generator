const chance = require('../../chance-inst');

const deviceType = 'light';

module.exports = {
  create: () => new ctor()
};

const onoff = ['On', 'Off'];
let idx = 0;
const nextOnOff = () => {
  idx = idx ? 0 : 1;
  return onoff[idx];
};

const streams = {
  onOff: () => nextOnOff(),
  voltage: () => chance.floating({min: 118, max: 121, fixed: 1}),
  currentLevel: () => chance.floating({min: 0, max: 101, fixed: 12}),
};

const streamNames = ['onOff', 'voltage', 'currentLevel'];
const streamWeights = [10, 2140, 10];

function ctor() {
  const id = chance.guid();

  this.getEvent = function(){
    let streamName = chance.weighted(streamNames, streamWeights);
    let streamValue = streams[streamName]();
    return {
      device: id,
      deviceType: deviceType,
      stream: streamName,
      streamValue: streamValue
    };
  };
}