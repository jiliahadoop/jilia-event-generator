const chance = require('../../chance-inst');

const deviceType = 'thermostat';

module.exports = {
  create: () => new ctor()
};

const streams = {
  activeMode: () => chance.pickone(['Idle', 'Cooling', 'Heating']),
  fanMode: () => chance.pickone(['Off', 'Low', 'Medium', 'High', 'On', 'Auto']),
  mode: () => chance.pickone(['Off', 'Cool', 'Heat', 'Auto', 'Emergency', 'Heat']),
  relayState: () => chance.pickone([
    'Fan', 'Cool', 'Heat',
    '2nd Stage Heat', '2nd Stage Cool',
    '2nd Stage Fan', '3rd Stage Fan'
  ]),
  batteryVoltage: () => chance.floating({min: 0.1, max: 3.1, fixed: 1}),
  coolingSetpoint: () => chance.floating({min: 20, max: 24, fixed: 2}),
  heatingSetpoint: () => chance.floating({min: 15, max: 20, fixed: 2}),
  currentTemperature: () => chance.floating({min: 12, max: 30, fixed: 2})
};

const streamNames = ['activeMode', 'fanMode', 'mode', 'relayState', 'batteryVoltage', 'coolingSetpoint', 'heatingSetpoint', 'currentTemperature'];
const streamWeights = [97, 1, 0.04, 97, 1, 1, 1, 250];

function ctor() {
  const id = chance.guid();

  this.getEvent = function(){
    let streamName = chance.weighted(streamNames, streamWeights);
    let streamValue = streams[streamName]();
    return {
      device: id,
      deviceType: deviceType,
      stream: streamName,
      streamValue: streamValue
    };
  };
}