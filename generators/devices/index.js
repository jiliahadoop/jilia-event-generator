module.exports = {
  thermostat: require('./thermostat'),
  contact: require('./contact'),
  occupancy: require('./occupancy'),
  relay: require('./relay'),
  dimmer: require('./dimmer')
};