'use strict';
const async = require('async');

const chance = require('../chance-inst');
const hubGenerator = require('./generate-hub');
const setup = require('../setup');
const nifiSocket = require('../socket');
const logger = require('../app-logger');

module.exports = {
  create: (name, numHubs) => new ctor(name, numHubs)
};

function ctor(name, numberHubs) {
  const me = name;
  this.getName = () => me;

  const hubs = (new Array(numberHubs)).fill(0).map(() => hubGenerator.create());

  const sockets = new Array(setup.SOCKETS_PER_TENANT)
    .fill(0).map(() => nifiSocket.create(me));

  let socketIdx = 0;
  const getNextSocket = () => {
    let max = sockets.length;
    let socket = sockets[socketIdx];
    while(!socket.canWrite() && max-- != 0) {
      // logger.logLine(`Socket ${socket.name()} not available, trying next socket`);
      socketIdx = (socketIdx + 1) % sockets.length;
      socket = sockets[socketIdx];
    }
    return socket.canWrite() ? socket : null;
  };

  let timers = [];
  this.openSockets = function(cb){
    logger.logLine(`${me} open sockets: ${sockets.length}`);
    async.eachSeries(sockets, (socket, next) => {
      socket.start(next);
    }, (err) => {
      if(err) {
        logger.logLine(`ERROR: Tenant ${me} socket connection error`);
        return cb(err);
      }
      logger.logLine(`${me} sockets opened`);
      cb(null);
    });
  };

  this.startHubs = function(interval, onEvent, cb){
    logger.log(`Starting ${me} hubs`);
    async.eachSeries(hubs, (hub, next) => {
      logger.log('.');
      startEventsTimer(interval, hub, onEvent);
      let delay = chance.integer({min: 250, max: 500});
      setTimeout(next, delay);
    }, () => {
      logger.logLine('done');
      cb(null);
    });
  };

  this.stopHubs = function(cb) {
    timers.forEach((t) => clearInterval(t));
    logger.logLine(`Closing ${me} open sockets`);
    async.each(sockets, (socket, nextSocket) => {
      socket.stop(nextSocket);
    }, cb);
  };

  function startEventsTimer(interval, hub, onEvent) {
    let timer = setInterval(() => {
      let evt = getEvent(me, hub);
      let socket = getNextSocket();
      if(!socket) {
        // logger.logLine(`${me} no sockets available`);
        return;
      }
      socket.onEvent(evt);
      onEvent(evt);
    }, interval);
    timers.push(timer);
  }
}

function getEvent(tenant, hub) {
  let hubEvt = hub.getEvent();
  return {
    'name': `devicedata.${hubEvt.deviceType}.${hubEvt.stream}`,
    'timestamp': Date.now(),
    'value': hubEvt.streamValue,
    'tenant': tenant,
    'tags': {
      'hub': hubEvt.hub,
      'device': hubEvt.device,
      'deviceType': hubEvt.deviceType,
      'stream': hubEvt.stream,
      'req-header-host': 'centralite.iot.apigee.net',
      'req-header-x-forwarded-for': '10.0.15.194',
      'req-header-x-forwarded-port': '12760',
      'req-header-x-forwarded-proto': 'https',
      'req-header-x-forwarded-host': 'api.jilia.io',
      'req-header-x-apigee-iot-tenant-id': tenant
    }
  };
}

