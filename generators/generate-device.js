'use strict';

const deviceGenerator = require('./devices');
module.exports = (deviceType) => deviceGenerator[deviceType].create();