'use strict';

const chance = require('../chance-inst');
const getNewDevice = require('./generate-device');

module.exports = {
  create: () => new ctor()
};

function ctor() {
  this.id = chance.guid();

  const therm = getNewDevice('thermostat');
  const contact = getNewDevice('contact');
  const dimmer = getNewDevice('dimmer');
  const relay = getNewDevice('relay');
  const occupancy = getNewDevice('occupancy');

  const devices = [
    therm, contact, dimmer, relay, occupancy
  ];
  const deviceWeights = [
    1.36, 13.79, 33.65, 29.92, 19.52
  ];

  this.getEvent = () => {
    let device = chance.weighted(devices, deviceWeights);
    let event = device.getEvent();
    event.hub = this.id;
    return event;
  };

}