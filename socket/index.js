'use strict';

const path = require('path');
const fs = require('fs');
const tls = require('tls');
const net = require('net');
const EOL = require('os').EOL;

const chance = require('../chance-inst');
const setup = require('../setup');
const logger = require('../app-logger');

const HOST = process.env.NIFI_HOST;
const PORT = parseInt(process.env.NIFI_PORT);
const MAX_RECONNECT_ATTEMPTS = setup.MAX_RECONNECT_ATTEMPTS;
const RECONNECT_ATTEMPT_DELAY = setup.RECONNECT_ATTEMPT_DELAY;

const SECURE_DIR = path.join(__dirname, 'connection');
const CA = path.join(SECURE_DIR, 'ca.cert');
const KEY_FILE = path.join(SECURE_DIR, 'key.private');
const CERT_FILE = path.join(SECURE_DIR, 'nifi-test-client.signed.cert');

const getConnectionFileContents = (filePath) => {
  if(setup.MOCK_SOCKET) return '';
  if(!setup.USE_TLS) return '';
  return fs.readFileSync(filePath);
};

const TLS_CONNECTION_OPTIONS = Object.freeze({
  key: getConnectionFileContents(KEY_FILE),
  cert: getConnectionFileContents(CERT_FILE),
  ca: [getConnectionFileContents(CA)],
  rejectUnauthorized: false
});

let counter = 0;

module.exports = {
  create: (tenantName) => {
    counter++;
    let fnCtor = setup.MOCK_SOCKET ? ctorMock : ctor;
    return new fnCtor(`${tenantName}-socket-${counter}`);
  }
};

function ctorMock(id) {
  const me = id;
  // let counter = 0;
  this.onEvent = () => {
    // do nada
    // counter++;
  };

  this.name = () => me;
  this.canWrite = () => true;
  this.start = (cb) => {
    logger.log(`Connecting Socket ${me}...`);
    setTimeout(() => {
      logger.logLine('done');
      cb(null);
    }, chance.integer({min: 1000, max: 2000}));
  };
  this.stop = () => {
    console.log(`Socket ${me} Connection Closed`);
  };
}

function ctor(id) {
  const me = id;
  const noop = () => {};

  let socket = null;
  let isOpen = false;
  let reconnectAttempts = 0;

  this.onEvent = (evt) => {
    if(!isOpen) return false;
    let msg = JSON.stringify(evt) + EOL;
    socket.write(msg);
    return true;
  };

  const connectSocket = () => {
    let result;
    if(setup.USE_TLS) {
      result = tls.connect(PORT, HOST, TLS_CONNECTION_OPTIONS);
    }
    else {
      result = net.connect(PORT, HOST);
    }
    // Ensure no buffering of data; send immediately
    result.setNoDelay(true);
    return result;
  };

  const reconnect = () => setTimeout(startReconnect, RECONNECT_ATTEMPT_DELAY);

  const startReconnect = () => {

    if(MAX_RECONNECT_ATTEMPTS >= 0 && reconnectAttempts >= MAX_RECONNECT_ATTEMPTS) {
      logger.logLine(`Socket ${me} Unable to Reconnect after ${reconnectAttempts} attempts`);
      return;
    }

    reconnectAttempts++;
    logger.logLine(`Socket ${me} Reconnecting (${reconnectAttempts})`);

    socket = connectSocket();
    socket.once('connect', () => {
      isOpen = true;
      reconnectAttempts = 0;
      logger.logLine(`Socket ${me} Reconnected`);
    });

    socket.once('close', () => {
      isOpen = false;
      logger.logLine(`Socket ${me} Close`);
      reconnect();
    });

    socket.once('error', (err) => {
      isOpen = false;
      logger.logLine(`Socket ${me} - ${err}`);
      reconnect();
    });

  };

  const startInitialConnection = (cb) => {

    socket = connectSocket();

    socket.once('connect', () => {
      isOpen = true;
      reconnectAttempts = 0;
      logger.logLine(`Socket ${me} Connected`);
      cb(null);
    });

    socket.once('close', () => {
      logger.logLine(`Socket ${me} Close`);
      // if not opened then initial connect failed and
      // the caller is waiting on a result
      if(!isOpen) return cb('Socket Could Not Open');

      isOpen = false;
      reconnect();
    });

    socket.once('error', (err) => {
      logger.logLine(`Socket ${me} - ${err}`);
      // if not opened then initial connect failed and
      // the caller is waiting on a result
      if(!isOpen) return cb(err);

      isOpen = false;
      reconnect();
    });

  };

  this.name = () => me;
  this.canWrite = () => isOpen;

  this.start = (cb) => {
    cb = cb || noop;
    startInitialConnection(cb);
  };

  this.stop = (cb) => {
    if(!isOpen) return cb(null);

    logger.logLine(`Stopping ${me}`);
    socket.removeAllListeners();

    socket.once('close', () => {
      cb(null);
      cb = noop;
    });
    setTimeout(() => {
      cb(null);
      cb = noop;
    }, 1000);

    socket.destroy();
  };


}