'use strict';
const setup = require('./setup');

const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const path = require('path');
const fs = require('fs');
const EOL = require('os').EOL;

require('console.table');
const prettyhr = require('pretty-hrtime');
const async = require('async');

const logger = require('./app-logger');
const chance = require('./chance-inst');
const tenantGenerator = require('./generators/generate-tenant');

const tenants = setup.TENANT_NAMES.map((name) => {
  return tenantGenerator.create(name, setup.HUBS_PER_TENANT);
});

const LOGS_DIR = path.join(process.cwd(), 'logs');
if(setup.LOG_EVENTS) {
  try {
    fs.mkdirSync(LOGS_DIR);
  }
  catch(ex) {
    if(ex.code !== 'EEXIST') throw ex;
  }
}
const logFiles = setup.TENANT_NAMES.reduce((result, name) => {
  if(!setup.LOG_EVENTS) return result;
  let filePath = path.join(LOGS_DIR, `${name}-events.jsonl`);
  result[name] = fs.createWriteStream(filePath, {flags: 'w'});
  return result;
}, {});

const eventsCounter = setup.TENANT_NAMES.reduce((result, name) => {
  result[name] = 0;
  return result;
}, {});

const askKeepGoing = (cb) => {
  if(!process.stdout.isTTY) return cb(null);
  logger.log('Do you want to continue? (y|n): ');
  rl.question('Do you want to continue? (y|n): ', (resp) => {
    resp = resp.trim().toLowerCase();
    logger.logLine(resp);

    rl.close();

    let keepGoing = resp === 'yes' || resp === 'y';
    if(!keepGoing) {
      return processExit();
    }

    cb(null);
  });
};
///////////////////////////////////////////////////////
// MAIN
///////////////////////////////////////////////////////
let tenantsStarted = false;
let startTime;

async.waterfall([

  // User Confirmation
  (next) => askKeepGoing(next),

  // Open All Sockets
  (next) => {
    async.eachSeries(tenants, (tenant, nextTenant) => {
      tenant.openSockets(nextTenant);
    },  next);
  },

  // Start All Events
  (next) => {
    startEventsCountTimer();

    startTime = process.hrtime();
    async.eachSeries(tenants, (tenant, nextTenant) => {
      let delay = chance.integer({min: 500, max: 2000});
      tenant.startHubs(setup.EVENT_INTERVAL, onEvent, () => {
        setTimeout(nextTenant, delay);
      });
    }, next);
  }

], (err) => {
  if(err) {
    logger.logLine(err);
    return shutdown();
  }
  tenantsStarted = true;
  logger.logLine('App Running');
});

///////////////////////////////////////////////////////

let eventCounterTimer;
function startEventsCountTimer() {
  // Once start stats display, need to update
  // console out
  const consoleOut = parseInt(process.env.CONSOLE_OUT) === 1;
  logger.logToConsole(consoleOut);
  updateEventsCounter();
  eventCounterTimer = setInterval(() => {
    updateEventsCounter();
  }, setup.STATS_UPDATE);
}

let exiting = false;
function onEvent(evt) {
  if(exiting) return;
  process.nextTick(() => {
    eventsCounter[evt.tenant]++;
    if(setup.LOG_EVENTS && !exiting) {
      logFiles[evt.tenant].write(JSON.stringify(evt) + EOL);
    }
  });
}

const NUM_DISPLAY_LINES = setup.TENANT_NAMES.length+6;
function updateEventsCounter() {
  let sum = 0;
  let data = setup.TENANT_NAMES.map((name) => {
    sum += eventsCounter[name];
    return {tenant: name, events: eventsCounter[name]};
  });
  data.push({
    tenant: 'total',
    events: sum
  });

  let endTime = process.hrtime(startTime);
  let timeDisplay = prettyhr(endTime);

  if(setup.DISPLAY_COUNTER) {
    readline.clearScreenDown(process.stdout, 0);
    console.table(`    Running ${timeDisplay}`, data);
    readline.moveCursor(process.stdout, 0, -(NUM_DISPLAY_LINES));
  }
  if(tenantsStarted){
    logger.logLine(`${timeDisplay} ${sum} events`);
  }
}

process.on('SIGINT', () => {
  logger.logLine('SIGINT');
  shutdown();
});

function shutdown() {
  exiting = true;
  readline.moveCursor(process.stdout, 0, NUM_DISPLAY_LINES);
  clearInterval(eventCounterTimer);

  async.waterfall([
    (next) => {
      async.each(tenants, (tenant, nextTenant) => {
        tenant.stopHubs(nextTenant);
      }, next);
    },
    (next) => closeEventLogs(next)
  ], processExit);
}

function closeEventLogs(cb) {
  if(!setup.LOG_EVENTS) return cb(null);

  logger.log('Closing event logs');
  let closedCount = 0;
  setup.TENANT_NAMES.forEach((name) => {
    logFiles[name].once('finish', () => {
      logger.log('.');
      closedCount++;
      if(closedCount === setup.TENANT_NAMES.length) {
        logger.logLine('done');
        cb(null);
      }
    });
    logFiles[name].end();
  });
}

function processExit(statusCode) {
  if(statusCode === undefined) statusCode = 0;
  logger.logLine('Exiting');
  logger.close();
  setTimeout(() => process.exit(statusCode), 250);
}